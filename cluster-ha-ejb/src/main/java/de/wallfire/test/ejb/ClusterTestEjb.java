package de.wallfire.test.ejb;

import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.annotation.PostConstruct;
import jakarta.ejb.Local;
import jakarta.ejb.Remote;
import jakarta.ejb.Schedule;
import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.LockModeType;
import jakarta.persistence.PersistenceContext;

/**
 * Session Bean implementation class ClusterTestEjb
 */
@Stateless(name = "ClusterTestEjb")
@Local({ ClusterTestEjbLocal.class })
@Remote({ ClusterTestEjbRemote.class })
public class ClusterTestEjb implements ClusterTestEjbLocal
{
    private static final Logger LOGGER = LoggerFactory.getLogger(ClusterTestEjb.class);
    private static final String NODE_NAME = System.getProperty("jboss.node.name", "undefined");
    private static final AtomicInteger INSTANCE_NUMBER = new AtomicInteger();

    private String instanceName;

    @PostConstruct
    private void init()
    {
        instanceName = NODE_NAME + "-" + INSTANCE_NUMBER.incrementAndGet();
        LOGGER.warn("#init() - instance: {}", instanceName);
    }

    @PersistenceContext(unitName = "mco-model-unit")
    private EntityManager entityManager;

    @Override
    public String getNodeName()
    {
        return instanceName;
    }

    @Override
    public int incrementAndGetCounter(String caller)
    {
        int oldCounter = 0;
        ClusterTestEntity clusterTestEntity = getCounterEntity();
        if(clusterTestEntity == null)
        {
            clusterTestEntity = new ClusterTestEntity();
            clusterTestEntity.setId(1L);
            oldCounter = clusterTestEntity.getCounter();
            clusterTestEntity.incrementAndGet();
            entityManager.persist(clusterTestEntity);
            LOGGER.warn(
                    "#incrementAndGetCounter(CREATE) - called on: {}, Counter (old): {}, Counter (new): {} | called by: {}",
                    getNodeName(),
                    oldCounter,
                    clusterTestEntity.getCounter(),
                    caller);
        }
        else
        {
            //entityManager.refresh(clusterTestEntity);
            oldCounter = clusterTestEntity.getCounter();
            clusterTestEntity.incrementAndGet();
            clusterTestEntity = entityManager.merge(clusterTestEntity);
            LOGGER.warn(
                    "#incrementAndGetCounter(UPDATE) - called on: {}, Counter (old): {}, Counter (new): {} | called by: {}",
                    getNodeName(),
                    oldCounter,
                    clusterTestEntity.getCounter(),
                    caller);
        }
        return clusterTestEntity.getCounter();
    }

    @Override
    public int getCounter()
    {
        ClusterTestEntity clusterTestEntity = getCounterEntity();
        if(clusterTestEntity == null)
        {
            return 0;
        }
        return clusterTestEntity.getCounter();
    }

    @Override
    public ClusterTestEntity getCounterEntity()
    {
        //return entityManager.find(ClusterTestEntity.class, 1L);
        return entityManager.find(ClusterTestEntity.class, 1L, LockModeType.PESSIMISTIC_WRITE);
    }

    @Schedule(hour = "*", minute = "*", second = "*/15", persistent = true)
    private void clusteredIncrementTimer()
    {
        LOGGER.warn(
                "#clusteredIncrementTimer() - Counter [incremented]: {} | caller: cluster-wide-timer-stateless-ejb-{}",
                incrementAndGetCounter("cluster-wide-timer-from-stateless-ejb"),
                getNodeName());
    }
}
