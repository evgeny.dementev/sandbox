package de.wallfire.test.ejb;

import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.Resource;
import jakarta.ejb.EJB;
import jakarta.ejb.LocalBean;
import jakarta.ejb.Schedule;
import jakarta.ejb.Singleton;
import jakarta.ejb.Startup;
import jakarta.ejb.Timeout;
import jakarta.ejb.Timer;
import jakarta.ejb.TimerConfig;
import jakarta.ejb.TimerService;

@LocalBean
@Singleton
@Startup
public class ClusterTimerTestEjb
{
    private static final Logger LOGGER = LoggerFactory.getLogger(ClusterTestEjb.class);
    private static final String NODE_NAME = System.getProperty("jboss.node.name", "undefined");

    @EJB(beanName = "ClusterTestEjb")
    private ClusterTestEjbRemote clusterTestEjb;

    public static String getNodeName()
    {
        return NODE_NAME;
    }

    @Schedule(hour = "*", minute = "*", second = "*/10", persistent = false)
    private void localIncrementTimer()
    {
        String nodeName = getNodeName();
        LOGGER.warn(
                "#localIncrementTimer() - Counter [incremented]: {} | caller: local-timer-singlton-ejb-{}",
                clusterTestEjb.incrementAndGetCounter("local-timer-singlton-ejb" + nodeName),
                nodeName);
    }

    @Schedule(hour = "*", minute = "*", second = "*/10", persistent = true)
    private void clusteredIncrementTimer()
    {
        String nodeName = getNodeName();
        LOGGER.warn(
                "#clusteredIncrementTimer() - Counter [current]: {} | caller: cluster-wide-timer-singlton-ejb-{}",
                clusterTestEjb.incrementAndGetCounter("cluster-wide-timer-singlton-ejb-" + nodeName),
                nodeName);
    }

    @Schedule(hour = "*", minute = "*", second = "*/5", persistent = false)
    private void localGetCounterTimer()
    {
        LOGGER.warn(
                "#localGetCounterTimer() - Counter [current]: {} | caller: local-timer-singlton-ejb-{}",
                clusterTestEjb.getCounter(),
                getNodeName());
    }

    @Schedule(hour = "*", minute = "*", second = "*/5", persistent = true)
    private void clusteredGetCounterTimer()
    {
        LOGGER.warn(
                "#clusteredGetCounterTimer() - Counter [current]: {} | caller: cluster-wide-timer-singlton-ejb-{}",
                clusterTestEjb.getCounter(),
                getNodeName());
    }
}
