package de.wallfire.test.ejb;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import jakarta.annotation.Resource;
import jakarta.ejb.EJB;
import jakarta.ejb.LocalBean;
import jakarta.ejb.ScheduleExpression;
import jakarta.ejb.Singleton;
import jakarta.ejb.Startup;
import jakarta.ejb.Timeout;
import jakarta.ejb.Timer;
import jakarta.ejb.TimerConfig;
import jakarta.ejb.TimerService;
import de.psi.go.ai.time.ExpressionConverter;

@LocalBean
@Singleton
@Startup
public class ClusterTimerServiceTestEjb
{
    private static final Logger LOGGER = LoggerFactory.getLogger(ClusterTimerServiceTestEjb.class);
    private static final String NODE_NAME = System.getProperty("jboss.node.name", "undefined");

    @EJB(beanName = "ClusterTestEjb")
    private ClusterTestEjbRemote clusterTestEjb;

    public static String getNodeName()
    {
        return NODE_NAME;
    }

    @Resource
    private TimerService timerService;
    private TimerConfig localTimerConfig;
    private TimerConfig clusterTimerConfig;

    @PostConstruct
    void init()
    {
        ScheduleExpression expression = ExpressionConverter.cronExpressionToScheduleExpression("*/5 * * * * *");
        localTimerConfig = new TimerConfig("local-timer-config", false);
        clusterTimerConfig = new TimerConfig("cluster-timer-config", true);
        timerService.createIntervalTimer(0L, 4000L, localTimerConfig);
        timerService.createCalendarTimer(expression, clusterTimerConfig);
    }

    @PreDestroy
    void destroy()
    {
        timerService.getAllTimers().forEach(Timer::cancel);
    }

    @Timeout
    void cycle(Timer timer)
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(timer.getInfo());
        stringBuilder.append("-");
        stringBuilder.append(getNodeName());
        LOGGER.warn(
                "#cycle() - Counter [incremented]: {} | caller: {}",
                clusterTestEjb.incrementAndGetCounter(stringBuilder.toString()),
                stringBuilder.toString());
    }
}
