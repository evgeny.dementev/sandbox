package de.wallfire.test.ejb;

import jakarta.persistence.Cacheable;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Cacheable
@Table(name = "ClusterWideCounter")
public class ClusterTestEntity
{
    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "counter", unique = true)
    private int counter;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public int getCounter()
    {
        return counter;
    }

    public void setCounter(int counter)
    {
        this.counter = counter;
    }

    public int incrementAndGet()
    {
        return ++this.counter;
    }

}
