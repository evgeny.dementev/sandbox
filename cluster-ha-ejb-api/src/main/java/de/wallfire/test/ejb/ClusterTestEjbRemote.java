package de.wallfire.test.ejb;

public interface ClusterTestEjbRemote
{
    String getNodeName();
    
    ClusterTestEntity getCounterEntity();

    int incrementAndGetCounter(String caller);

    int getCounter();
}
