package de.wallfire.test.web;

import java.io.IOException;

import de.wallfire.test.ejb.ClusterTestEjbRemote;
import jakarta.ejb.EJB;
import jakarta.servlet.Servlet;
import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ClusterTestServlet
 */
@WebServlet(loadOnStartup = 1, value = "/counter")
public class ClusterTestServlet extends HttpServlet
{
    private static final long serialVersionUID = 1L;
    private static final String NODE_NAME = System.getProperty("jboss.node.name", "undefined");

    public static String getNodeName()
    {
        return NODE_NAME;
    }

    @EJB(beanName = "ClusterTestEjb")
    private ClusterTestEjbRemote clusterTestEjb;

    /**
     * @see Servlet#init(ServletConfig)
     */
    public void init(ServletConfig config) throws ServletException
    {
        // TODO Auto-generated method stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String parameter = request.getParameter("increment");
        if(parameter != null)
        {
            response.getWriter().append("Served at: ").append(request.getContextPath())//
                    .append(", Caller: cluster-servlet-").append(getNodeName())//
                    .append(", Called on: ").append(clusterTestEjb.getNodeName())//
                    .append(", Counter [incremented]: ").append(
                            String.valueOf(clusterTestEjb.incrementAndGetCounter("cluster-servlet-" + getNodeName())));
        }
        else
            response.getWriter().append("Served at: ").append(request.getContextPath())//
                    .append(", Caller: cluster-servlet-").append(getNodeName())//
                    .append(", Called on: ").append(clusterTestEjb.getNodeName())//
                    .append(", Counter [current]: ").append(String.valueOf(clusterTestEjb.getCounter()));
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        doGet(request, response);
    }

}
