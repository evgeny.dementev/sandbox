package de.wallfire.test.web;

import de.wallfire.test.ejb.ClusterTestEjbRemote;
import jakarta.ejb.EJB;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

@Path("/counter")
@ApplicationScoped
public class ClusterTestResource
{
    private static final String NODE_NAME = System.getProperty("jboss.node.name", "undefined");
    @EJB(beanName = "ClusterTestEjb")
    private ClusterTestEjbRemote clusterTestEjb;

    public static String getNodeName()
    {
        return NODE_NAME;
    }

    @GET
    @Path("/get")
    @Produces(MediaType.TEXT_PLAIN)
    public String getCounter()
    {
        StringBuilder builder = new StringBuilder("Called on: ");
        builder.append(clusterTestEjb.getNodeName());
        builder.append(", Counter current: ");
        builder.append(clusterTestEjb.getCounter());
        return builder.toString();
    }

    @GET
    @Path("/increment")
    @Produces(MediaType.TEXT_PLAIN)
    public String updateCounter()
    {
        StringBuilder builder = new StringBuilder("Called on: ");
        builder.append(clusterTestEjb.getNodeName());
        builder.append(", Counter after increment: ");
        builder.append(clusterTestEjb.incrementAndGetCounter("increment-from-rest-" + getNodeName()));

        return builder.toString();
    }

}
