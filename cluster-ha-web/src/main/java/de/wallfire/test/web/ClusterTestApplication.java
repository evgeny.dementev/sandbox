package de.wallfire.test.web;

import jakarta.ws.rs.ApplicationPath;
import jakarta.ws.rs.core.Application;

@ApplicationPath("/")
public class ClusterTestApplication extends Application
{

}
